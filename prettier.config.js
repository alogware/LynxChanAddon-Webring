module.exports = {
    printWidth: 120,
    endOfLine: "auto",

    semi: true,
    singleQuote: false,
    tabWidth: 4,
    useTabs: false,
    quoteProps: "consistent",
    trailingComma: "all",
    bracketSpacing: true,
    arrowParens: "avoid",

    jsxSingleQuote: false,
    bracketSameLine: false,
};
