//Loading add-on, at this point its safe to reference engine components
// @ts-check
"use strict";

const config = require("./config");

const mongodb = require("mongodb-legacy");
const http = require("http");
const https = require("https");
/** @type {import("cluster").default} */
// @ts-expect-error TypeScript doesn't understand that we aren't in module mode
const cluster = require("cluster");
const fs = require("fs");
const path = require("path");
const url = require("url");

const settingsHandler = require("../../settingsHandler");
const settings = settingsHandler.getGeneralSettings();

const requestHandler = require("../../engine/requestHandler");
const miscPages = require("../../engine/domManipulator").dynamicPages.miscPages;
const lang = require("../../engine/langOps").languagePack;
const getTemplates = require("../../engine/templateHandler").getTemplates;
const common = require("../../engine/domManipulator").common;
const miscOps = require("../../engine/miscOps");

const db = require("../../db");
/** @type {mongodb.Collection<LynxChanBoard>} */
const boards = db.boards();

const SPIDER_CACHE_FILE = "spider_cache.txt";

/**
 * A board used in the webring data.
 * @typedef {Object} WebringBoard
 * @property {string} uri - The board URI
 * @property {string} title - The board title/name
 * @property {string=} subtitle - The board subtitle/description
 * @property {string} path - The URL for the board
 * @property {number=} postsPerHour - The number of posts in the last hour
 * @property {number=} totalPosts - The total amount of posts on the board
 * @property {number=} uniqueUsers - The number of unique ISPs on the board
 * @property {boolean} nsfw - Whether this imageboard is Not Safe For Work
 * @property {string[]=} tags - The tags for the board
 */

/**
 * The webring data.
 *
 * @typedef {Object} Webring
 * @property {string} name - The name of the imageboard
 * @property {string} url - The base site URL
 * @property {string[]} logo - The imageboard's logos, an array of images.
 * @property {string[]} following - The other nodes this node is connected to.
 * @property {string[]} blacklist - The nodes this node will not connect to.
 * @property {WebringBoard[]} boards - The boards on the site
 */

/**
 * "Normalized" webring data, used by LynxChan to display webring boards on the boards list.
 *
 * @typedef {Object} NormalizedWebringBoard
 * @property {string} boardUri - The board URI
 * @property {string} boardName - The board title/name
 * @property {string} boardDescription - The board subtitle/description
 * @property {string} absPath - The URL for the board
 * @property {string | number} postsPerHour - The number of posts in the last hour
 * @property {string | number} lastPostId - The total amount of posts on the board
 * @property {string | number} uniqueIps - The number of unique ISPs on the board
 * @property {string} ibName - The name of the imageboard
 * @property {string[]} [tags] - The tags for the board
 */

/**
 * An extension module for the webring.
 *
 * @typedef {Object} WebringExtension
 * @property {string} id - The ID of the extension
 * @property {() => void} [init] - Initialize the extension
 * @property {(data: Webring[]) => void} webringFetched - Called when the webring data is fetched
 * @property {() => Object} [webringRequest] - Called when a webring request is made. Should return data to be included in the webring JSON.
 * @property {() => Object} [webringInternalRequest] - Called when a local webring request is made. Should return data to be included in the JSON.
 */

/**
 * A message sent from a worker to the master.
 *
 * @typedef {MasterWebringUpdateRequest | MasterExtensionHook} MasterMessage
 */

/**
 * A webring update request, sent from a worker to the master.
 *
 * @typedef {Object} MasterWebringUpdateRequest
 * @property {true} webringUpdate - Request an update of the webring data
 */

/**
 * A request to call an extension hook, sent from a worker to the master.
 *
 * @typedef {Object} MasterExtensionHook
 * @property {Exclude<keyof WebringExtension, "id">} extensionHook - The name of the hook
 * @property {Parameters<Exclude<WebringExtension[Exclude<keyof WebringExtension, "id">], undefined>>} [args] - The arguments to call the hook with
 * @property {string} hookID - The ID of the hook
 */

/**
 * A message sent from the master to a worker.
 *
 * @typedef {WorkerWebringUpdate | WorkerExtensionHookResponse} WorkerMessage
 */

/**
 * A webring update message, sent from the master to a worker.
 *
 * @typedef {Object} WorkerWebringUpdate
 * @property {true} webringUpdate - Update the webring data
 * @property {Webring[]} data - The webring data
 * @property {NormalizedWebringBoard[]} normalizedData - The normalized webring data
 * @property {string[]} spiderCache - The spider cache
 */

/**
 * The response of the extension hook call, sent from the master to a worker.
 *
 * @typedef {Object} WorkerExtensionHookResponse
 * @property {string} extensionHookResponse - The ID of the hook
 * @property {any} extensionData - The data returned by the extension. This is untyped, but {@link queueHook} already
 *                                 ensures that the callback is called with the correct type.
 */

/**
 * A LynxChan board in the database.
 *
 * @typedef {Object} LynxChanBoard
 * @property {string} boardUri - The board URI
 * @property {string} boardName - The board title/name
 * @property {string} boardDescription - The board subtitle/description
 * @property {string[]} [settings] - Settings for the board
 * @property {string[]} [specialSettings] - Special settings for the board
 * @property {string[]} [tags] - The tags for the board
 * @property {Date} [lastPostDate] - The date of the last post
 * @property {number} [postsPerHour] - The number of posts in the last hour
 * @property {number} [lastPostId] - The total amount of posts on the board
 * @property {number} [uniqueIps] - The number of unique ISPs on the board
 */

/**
 * Generate JSON corresponding to the current state of the webring and send it to the client.
 *
 * @param {http.ClientRequest} req - The request
 * @param {http.ServerResponse} res - The response
 * @param {Set<string>} spiderCache - The set of crawled webring URLs
 * @param {Record<string, any>} extensionData - The data returned by the extension hooks
 * @interface public
 */
const generateWebringJSON = (req, res, spiderCache, extensionData) => {
    boards.find({}).toArray((error, results) => {
        if (error) {
            // TODO graceful handling
            throw error;
        } else {
            if (results === undefined) throw new Error("Results is undefined");
        }

        const data = {
            name: settings.siteTitle,
            url: config.url,
            endpoint: config.url + "/webring.json",
            logo: config.logos,

            following: config.following,
            known: [...spiderCache],
            blacklist: [...config.blacklist],

            boards: results
                .filter(board => !board.settings || !board.settings.includes("unindex"))
                .map(board => ({
                    uri: board.boardUri,
                    title: board.boardName,
                    subtitle: board.boardDescription,
                    path: config.url + "/" + board.boardUri + "/",

                    postsPerHour: board.postsPerHour,
                    totalPosts: board.lastPostId,
                    uniqueUsers: board.uniqueIps,
                    nsfw: !(board.specialSettings && board.specialSettings.indexOf("sfw") > -1),
                    tags: board.tags?.map(tag => tag.trim()) ?? [],
                    lastPostTimestamp: board.lastPostDate ? board.lastPostDate.toISOString() : null,
                })),

            extensions: extensionData,
        };

        res.writeHead(200, {
            "Content-Type": "application/json",
        });
        res.end(JSON.stringify(data));
    });
};

/**
 * Implementation of https.request using Promises. This allows usage of Promise utilities like Promise.all.
 *
 * @param {string|url.URL} requestURL - The path
 * @param {https.RequestOptions} [options] - request options
 * @return {Promise<string>}
 */
const requestAsync = (requestURL, options) =>
    new Promise((resolve, reject) => {
        /**
         * @param {http.IncomingMessage} res - The response
         */
        const callback = res => {
            if (res.headers["content-type"] !== "application/json") {
                reject(new Error("Webring data is not JSON"));
            }

            let accumulator = "";

            res.on("data", chunk => {
                accumulator += chunk;
            });

            res.on("end", () => {
                resolve(accumulator);
            });
        };

        if (typeof requestURL === "string") {
            requestURL = new url.URL(requestURL);
        }

        // Pick the correct module because Node is a stupid fucking piece of broken garbage.
        // Node.js has two separate modules for http/https. If you give one an URL with the
        // other protocol, it just throws an exception. Node.js people are truly braindead.
        const moduleToUse = requestURL.protocol === "https:" ? https : http;

        /** @type {http.ClientRequest} */
        let req;
        if (options) {
            req = moduleToUse.request(requestURL, options, callback);
        } else {
            req = moduleToUse.request(requestURL, callback);
        }

        req.on("error", reject);
        req.end();
    });

/**
 * Fetch data from all remote webrings. Call {@link callback} with the data when done.
 *
 * @param {Set<string>} spiderCache - the cache for the webring spiderweb
 * @param {(ibs: Webring[]) => void} callback - The callback
 */
const fetchRemoteWebring = (spiderCache, callback) => {
    /** @type {Webring[]} */
    const remoteBoards = [];

    /** @type {Set<string>} */
    const toVisit = new Set([...spiderCache].concat(config.following));
    /** @type {Set<string>} */
    const seenDomains = new Set();

    // make sure i'm not following myself
    toVisit.add(config.url + "/webring.json");
    seenDomains.add(config.url + "/webring.json");

    (function inner() {
        // collect all domains that we didn't go to yet
        /** @type {string[]} */
        const notVisited = [...toVisit].filter(url => !seenDomains.has(url));

        if (!notVisited.length) {
            callback(remoteBoards);
            return;
        }

        // do all the requests at once...
        const requests = notVisited.map(url =>
            requestAsync(url, {
                headers: {
                    "Accept": "application/json",
                    "X-Webring": "Ahoy!",
                },
            })
                .then(data => {
                    /** @type {Webring} */
                    const webring = JSON.parse(data);
                    remoteBoards.push(webring);

                    spiderCache.add(url);
                    // make sure we remember that we checked this domain
                    seenDomains.add(url);
                    // filter the new domains using the blacklist
                    return (webring.following || []).filter(url => !config.blacklist.some(i => url.includes(i)));
                })
                .catch(err => {
                    console.warn("[Webring] Couldn't parse webring data from", url);
                    console.warn(err);

                    // to avoid repeated requests, delete the domain from the sets
                    toVisit.delete(url);
                    seenDomains.delete(url);
                    return [];
                }),
        );

        // ...and wait for the results
        Promise.all(requests).then(newNodes => {
            newNodes.forEach(nodes => nodes.forEach(node => toVisit.add(node)));

            if (toVisit.size > seenDomains.size) {
                // if new domains were collected, recurse
                inner();
            } else {
                callback(remoteBoards);
            }
        });
    })();
};

/**
 * Take in raw webring data and normalize it into a list of boards for LynxChan.
 *
 * @param {Webring[]} data - The webring data
 * @return {NormalizedWebringBoard[]}
 */
const normalizeWebrings = data => {
    /** @type {NormalizedWebringBoard[]} */
    const output = [];

    data.forEach(ib => {
        ib.boards.forEach(board => {
            output.push({
                boardUri: board.uri,
                boardName: board.title,
                boardDescription: board.subtitle || "",

                postsPerHour: board.postsPerHour || "???",
                lastPostId: board.totalPosts || "???",
                uniqueIps: board.uniqueUsers || "???",

                ibName: ib.name,
                absPath: board.path,
            });
        });
    });

    return output;
};

/**
 * Unescape HTML entities.
 *
 * LynxChan for some reason saves the board name and description with HTML entities escaped, so we need to unescape them.
 * Doing this at the frontend is a bad idea because it's a potential XSS vector.
 * Frontends that use webring internal data should always use textContent or similar to render the data, and never
 * innerHTML.
 *
 * @param {string} str - The string to unescape
 * @return {string} the unescaped string
 */
const htmlUnescape = str =>
    str
        .replace(/&amp;/g, "&")
        .replace(/&lt;/g, "<")
        .replace(/&gt;/g, ">")
        .replace(/&quot;/g, '"')
        .replace(/&apos;/g, "'");

/**
 * Render the internal webring data for the dropdown.
 *
 * @param {Webring[]} webringData - The webring data
 * @param {Record<string, any>} extensionData - The data returned by the extension hooks for the internal endpoint
 * @param {(data: any) => void} callback - The callback
 */
const webringInternal = (webringData, extensionData, callback) => {
    boards
        .find(
            {},
            {
                projection: {
                    _id: 0,
                    boardUri: 1,
                    boardName: 1,
                    boardDescription: 1,
                    postsPerHour: 1,
                    lastPostId: 1,
                    uniqueIps: 1,
                    specialSettings: 1,
                    tags: 1,
                    lastPostDate: 1,
                },
            },
        )
        .toArray((error, results) => {
            if (error) {
                // TODO graceful handling
                throw error;
            } else {
                if (results === undefined) throw new Error("Results is undefined");
            }

            const data = {
                imageboards: [
                    // this imageboard
                    {
                        name: settings.siteTitle,
                        url: config.url,
                        logo: config.logos,
                        local: true,
                        boards: results.map(board => ({
                            uri: board.boardUri,
                            title: htmlUnescape(board.boardName),
                            subtitle: board.boardDescription,
                            path: config.url + "/" + board.boardUri + "/",
                            postsPerHour: board.postsPerHour,
                            totalPosts: board.lastPostId,
                            uniqueUsers: board.uniqueIps,
                            nsfw: !(board.specialSettings && board.specialSettings.indexOf("sfw") > -1),
                            tags: board.tags?.map(tag => tag.trim()) ?? [],
                            lastPostTimestamp: board.lastPostDate ? board.lastPostDate.toISOString() : null,
                        })),
                    },

                    // the rest
                    ...webringData.map(ib => ({
                        name: ib.name,
                        url: ib.url,
                        logo: ib.logo,
                        boards: ib.boards.map(board => ({
                            ...board,
                            tags: board.tags?.map(tag => tag.trim()) ?? [],
                        })),
                    })),
                ],

                extensions: extensionData,
            };

            callback(data);
        });
};

/**
 * Initialize the extensions listed in the configuration.
 *
 * @return {WebringExtension[]} the extension modules
 */
function initializeExtensions() {
    /** @type {WebringExtension[]} */
    const extensions = [];

    config.extensions.forEach(extensionPath => {
        /** @type {WebringExtension} */
        let extension;
        try {
            extension = require(extensionPath);
        } catch (e) {
            console.warn("[Webring] Extension", extensionPath, "couldn't be imported.");
            console.warn(e);
            return;
        }

        if (!("id" in extension)) {
            console.error("[Webring] Extension in", extensionPath, "doesn't have an extension ID. Discarded.");
            return;
        }

        try {
            extension.init?.();
        } catch (e) {
            console.warn("[Webring] Extension", extension.id, "failed during initialization.");
            console.warn(e);
        }

        extensions.push(extension);
    });

    return extensions;
}

/**
 * Call a specific method of all currently-loaded extensions.
 *
 * @template {Exclude<keyof WebringExtension, "id">} T - The name of the function to call
 * @param {WebringExtension[]} extensions - The extensions
 * @param {T} functionName - The name of the function to call
 * @param {Parameters<Exclude<WebringExtension[T], undefined>>} args - The arguments to call it with
 * @return {Record<string, any>} an object with each extension's response indexed by its extension ID.
 *                               If an error is thrown during the hook then it is not added.
 */
const callExtensionHook = (extensions, functionName, args) => {
    /** @type {Record<string, any>} */
    const extensionData = {};

    extensions.forEach(extension => {
        const fn = extension[functionName];
        if (fn !== undefined) {
            try {
                // @ts-expect-error This is a dynamic call, so we can't really typecheck it. TypeScript complains about
                // it with something like:
                // The 'this' context of type ... is not assignable to method's 'this' of type ...
                extensionData[extension.id] = fn.apply(extension, args);
            } catch (e) {
                console.warn("[Webring] Extension", extension.id, "failed during", functionName + ".");
                console.warn(e);
            }
        }
    });

    return extensionData;
};

/**
 * Ask the cluster's master to call extension hooks and respond with the data.
 *
 * @template {Exclude<keyof WebringExtension, "id">} T - The name of the function to call
 * @param {Record<string, (data: any) => void>} extensionCallbacks - The extension response callbacks
 * @param {T} functionName - The name of the function to call
 * @param {Parameters<Exclude<WebringExtension[T], undefined>>} args - The arguments to call it with
 * @param {(data: ReturnType<Exclude<WebringExtension[T], undefined>>) => void} callback - The callback which will be called when extension data is returned
 */
const queueHook = (extensionCallbacks, functionName, args, callback) => {
    if (!cluster.isWorker || process.send === undefined) {
        throw new Error("[Webring] Master attempted to call hook " + functionName + " by queue?!");
    }

    const hookID = Math.floor(Math.random() * 1000000).toString();

    extensionCallbacks[hookID] = callback;
    process.send({
        upStream: true,
        extensionHook: functionName,
        args,
        hookID,
    });
};

/**
 * Read the spider cache file and return the list of lines in it.
 *
 * @return {Set<string>} the list of lines in the file
 */
function readSpiderCache() {
    return new Set(
        fs
            .readFileSync(path.resolve(__dirname, SPIDER_CACHE_FILE), {
                flag: "a+",
            })
            .toString()
            .split("\n")
            .filter(l => l && !l.startsWith("#")),
    );
}

/**
 * Write the list of crawled webring URLs to the spider cache.
 *
 * @param {Iterable<string>} urls - The URLs to write
 * @return {void}
 */
function writeSpiderCache(urls) {
    fs.open(path.resolve(__dirname, SPIDER_CACHE_FILE), "w", (err, fd) => {
        if (err) {
            console.error("[Webring] Couldn't open " + SPIDER_CACHE_FILE + "!");
            console.error(err);
            fs.close(fd, err => {
                // something's really really wrong
                if (err) throw err;
            });
        } else {
            fs.write(fd, [...urls].join("\n"), (err, written) => {
                if (err) {
                    console.error("[Webring] Couldn't write " + SPIDER_CACHE_FILE + "!");
                    console.error(err);
                } else {
                    console.info("[Webring] Wrote", SPIDER_CACHE_FILE, "(now", written, "bytes)");
                }
                fs.close(fd, err => {
                    // something's really really wrong
                    if (err) throw err;
                });
            });
        }
    });
}

module.exports = {
    engineVersion: "2.9",

    init() {
        // Check if we are supposed to be a daemon first!
        // @ts-expect-error This is added after the fact, so we can't typecheck it
        if (require("../../argumentHandler").informedArguments.noDaemon.informed) return;

        // the original webring data
        /** @type {Webring[]|null} */
        let webringData = null;
        // the normalized webring data
        /** @type {any[]|null} */
        let normalizedData = null;
        // The spider cache. This is obtained in two ways:
        // - In the master, we read the cache file directly, and try to crawl it first, sending it afterwards
        //   to the workers.
        // - In the workers, we wait for the master to send the cache. If we receive a request while the
        //   cache is still updating, we send a webringUpdateRequest to the master, which will give us
        //   whatever's available in that moment. Once the crawl is done, the master will send the updated
        //   cache to all workers.
        /** @type {Set<string>|null} */
        let spiderCache = cluster.isPrimary ? readSpiderCache() : null;
        // The extension modules. This will only be handled by the cluster master.
        /** @type {WebringExtension[]|null} */
        let extensions = null;
        // (Worker only) The extension response callbacks.
        /** @type {Record<string, (data: any) => void>} */
        const extensionCallbacks = {};

        // add new routes
        const decideRouting = requestHandler.decideRouting;
        requestHandler.decideRouting = (req, pathName, res, callback) => {
            if (pathName === "/webring.json") {
                queueHook(extensionCallbacks, "webringRequest", [], extensionData => {
                    generateWebringJSON(req, res, spiderCache ?? new Set(), extensionData);
                    callback();
                });

                return;
            }

            // internal boards data used by the dropdown
            if (pathName === "/addon.js/webring") {
                res.writeHead("200", miscOps.getHeader("application/json"));

                queueHook(extensionCallbacks, "webringInternalRequest", [], extensionData => {
                    webringInternal(webringData || [], extensionData, results => {
                        res.write(JSON.stringify(results));

                        res.end();
                        callback();
                    });
                });

                return;
            }

            decideRouting(req, pathName, res, callback);
        };

        /**
         * @param {NormalizedWebringBoard} board - The board to render as a cell
         * @param {Object} language - The language pack in use
         */
        miscPages.getBoardCell = (board, language) => {
            const cellTemplate = getTemplates(language).boardsCell;
            const boardUri = common.clean(board.boardUri);

            let cell = '<div class="boardsCell">' + cellTemplate.template;
            cell += "</div>";

            const linkContent =
                (board.ibName ? board.ibName : "") + ("/" + boardUri + "/ - ") + common.clean(board.boardName);

            // handle board link if it's a webring board
            cell = cell.replace("__linkBoard_href__", board.ibName ? board.absPath : "/" + boardUri + "/");
            cell = cell.replace("__linkBoard_inner__", linkContent);

            cell = miscPages.setSimpleBoardCellLabels(board, cell, cellTemplate.removable);

            if (board.tags) {
                cell = cell.replace("__labelTags_location__", cellTemplate.removable.labelTags);

                cell = cell.replace("__labelTags_inner__", common.clean(board.tags.join(", ")));
            } else {
                cell = cell.replace("__labelTags_location__", "");
            }

            return miscPages.setBoardCellIndicators(cell, cellTemplate.removable, board);
        };

        // add webring boards to boards.js
        /**
         * @param {{ page: number }} parameters - The parameters for the page
         * @param {NormalizedWebringBoard[]} boards - The boards to render
         * @param {number} pageCount - The number of pages
         * @param {Object} language - The language pack in use
         */
        miscPages.boards = (parameters, boards, pageCount, language) => {
            const template = getTemplates(language).boardsPage;

            const document = miscPages.setOverboardLinks(template).replace("__title__", lang(language).titBoards);

            const webringSkip = normalizedData ? settings.boardsPerPage * ((parameters.page || 1) - 1) : 0;

            // top spaghetti
            return miscPages
                .setBoards(
                    boards.concat(
                        normalizedData ? normalizedData.slice(webringSkip, webringSkip + settings.boardsPerPage) : [],
                    ),
                    document,
                    language,
                )
                .replace("__divPages_children__", miscPages.getPages(parameters, pageCount));
        };

        // webring updates and sending to workers
        if (cluster.isPrimary) {
            const cache = /** @type {Set<string>} */ (spiderCache);
            // set up extensions
            const exts = (extensions = initializeExtensions());

            // set up main thread to pull the webring data
            const updateData = () => {
                fetchRemoteWebring(cache, ibs => {
                    webringData = ibs;
                    normalizedData = normalizeWebrings(ibs);

                    const workers = Object.values(/** @type {Record<string, cluster.Worker>} */ (cluster.workers));
                    workers.forEach(worker => {
                        worker.send({
                            webringUpdate: true,
                            data: webringData,
                            normalizedData,
                            spiderCache: [...(spiderCache ?? [])],
                        });
                    });

                    callExtensionHook(exts, "webringFetched", [webringData]);

                    console.info("[Webring] Updated webring data");
                    writeSpiderCache(cache);
                });
                // update every 15 minutes
                setTimeout(updateData, 15 * 60 * 1000);
            };
            updateData();
        } else {
            if (process.send === undefined) {
                throw new Error("[Webring] Worker started without a master?");
            }

            if (!webringData) process.send({ upStream: true, webringUpdateRequest: true });
        }

        // worker threads get the data from master
        // this is required because lynxchan worker boot is slow
        // enough that the webring remote fetch can outpace
        // worker boot.
        // Also, for some god-forsaken reason I can't attach more than
        // one listener to the workers. Probably because workers haven't
        // booted when this gets called on master. So I have to override
        // the message processor.
        const kernel = require("../../kernel");
        const processTopDownMessage = kernel.processTopDownMessage;
        /**
         * @param {MasterMessage | WorkerMessage} masterOrWorkerMessage
         */
        kernel.processTopDownMessage = masterOrWorkerMessage => {
            if (cluster.isPrimary) {
                let workers = Object.values(/** @type {Record<string, cluster.Worker>} */ (cluster.workers));
                const message = /** @type {MasterMessage} */ (masterOrWorkerMessage);

                if ("webringUpdateRequest" in message && message.webringUpdateRequest) {
                    workers.forEach(worker => {
                        worker.send({
                            webringUpdate: true,
                            data: webringData,
                            normalizedData,
                            spiderCache: [...(spiderCache ?? [])],
                        });
                    });
                    return;
                }

                if ("extensionHook" in message) {
                    // TODO: Instead of sending to all workers, we should send to the worker that requested the hook
                    workers.forEach(worker => {
                        const args = message.args ?? [];

                        worker.send({
                            extensionHookResponse: message.hookID,
                            extensionData:
                                extensions !== null ? callExtensionHook(extensions, message.extensionHook, args) : null,
                        });
                    });
                    return;
                }
            } else if (cluster.isWorker) {
                const message = /** @type {WorkerMessage} */ (masterOrWorkerMessage);

                if ("webringUpdate" in message && message.webringUpdate) {
                    webringData = message.data;
                    normalizedData = message.normalizedData;
                    spiderCache = new Set(message.spiderCache);
                    return;
                }

                if ("extensionHookResponse" in message) {
                    const id = message.extensionHookResponse;
                    const callback = extensionCallbacks[id];

                    if (callback) {
                        callback(message.extensionData);
                        delete extensionCallbacks[id];
                    } else {
                        // Currently the method the master uses to send the response is spray-and-pray, so we need to
                        // ignore responses for hooks that we didn't request.
                        if (settings.verbose) {
                            console.debug("[Webring] Received extension data for unknown hook ID", id);
                        }
                    }

                    return;
                }
            }

            processTopDownMessage(masterOrWorkerMessage);
        };

        // update the last post time for board.
        const postingCommon = require("../../engine/postingOps/common");
        const addPostToStats = postingCommon.addPostToStats;
        postingCommon.addPostToStats = (ip, boardUri, callback) => {
            boards
                .findOneAndUpdate(
                    {
                        boardUri,
                    },
                    {
                        $set: {
                            lastPostDate: new Date(),
                        },
                    },
                )
                .then(() => addPostToStats(ip, boardUri, callback));
        };
    },
};
